## Máxima diferencia entre elementos de lista

Define la función `maximum_difference` que recibe como argumento una lista de números y retorna la maxima diferencia de esa lista. Los resultados de las comparaciones en el `driver code` deben ser `true`.

Un ejemplo para obtener la maxima diferencia de una lista es el siguiente:

Dada la siguiente lista [1, 2, 5], las operaciones para obtener la máxima diferencia serán:

- Primera resta con valor absoluto:

  |1-2| = 1 

- Segunda resta con valor absoluto:

  |1-5| = 4

- Tercer resta con valor absoluto:

  |2-5| = 3

La maxima diferencia es 4.


```python
"""maximum difference function"""

def maximum_difference(list_of_numbers):
    pass



#driver code
print(maximum_difference([1, 2, 5, 0]) == 5)
print(maximum_difference([1, 2, 5]) == 4)
```
