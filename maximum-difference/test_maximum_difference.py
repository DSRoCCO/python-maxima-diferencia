import unittest
from maximum_difference import *

class MaximumDifferenceTests(unittest.TestCase):

	def test_list_four_values(self):
		self.assertEqual(maximum_difference([1, 2, 5, 0]), 5)

	def test_list_three_values(self):
		self.assertEqual(maximum_difference([1, 2, 5]), 4)

if __name__=="__main__":
    unittest.main()